/**
 * HTTP Library
 * for making HTTP request 
 */

 //import { UI } from './ui';

 class EasyHttP{
   // Make an Http Get request 
   get(url){
      return new Promise((resolve, reject) => {
         fetch(url)
         .then(res => res.json())
         .then(data => resolve(data))
         .catch(err => reject(err))
      })
      
    }
 }