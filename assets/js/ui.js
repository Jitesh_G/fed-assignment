class UI{
   constructor(){
      this.movie = document.querySelector('#movies');
      this.movieDetail = document.querySelector('#movieDetail');
   }
   showMovies(movies){
      let output = '';
      movies.forEach(movie => {
         output += `
        <div class="movie-container col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <div class="movie mb-3">
               <div class="movie__poster" id="${movie._id}" data-item="${movie.slug}">
                  <img class="poster__image" src="https://image.tmdb.org/t/p/w500${movie.posterURL}" alt="">
                  <div class="poster__overlay">
                     <i class="fa fa-link"></i>
                  </div>
               </div>
               <div class="moview__details">
                  <h3 class="movie__title">${movie.title}</h3>
                  <h5 class="movie__date"><i class="fa fa-calendar"></i> <span>${movie.releaseDate}</span></h5>
               </div>
            </div>
        </div>`;
         
      });
      this.movie.innerHTML = output;
    }

    showMovie(movie){
      let output = `
            <div class="movie-detail">
            <div class="banner" style="background:url('https://image.tmdb.org/t/p/w1280${movie.backdropURL}') no-repeat center top;background-size:cover"></div>
            <div class="movie-details container">
               <div class="movie-title">
               <span class="back"><i class="fa fa-reply" aria-hidden="true"></i></span>
               <img src="https://image.tmdb.org/t/p/w500${movie.posterURL}" alt="${movie.title}">
                  <div class="title">
                     <h2>${movie.title}</h2>
                     <h3><i class="fa fa-calendar"></i> <span>${movie.releaseDate}</span></h3>
                  </div>
               </div>
               <div class="movie-description">
                  <p>${movie.plot}</p>
               </div>
            </div>
         </div>
      `;
      history.replaceState(null, null,"?title="+movie.slug);
      this.movie.innerHTML = '';
      this.movieDetail.innerHTML = output;
     
      document.querySelector('.back').addEventListener('click', loadPage);
      function loadPage(){
         window.history.pushState({}, null, "/" + "");
         location.reload();

      }
      
    }
    
}
