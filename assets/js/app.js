//for loader
window.onload = function(){
   document.querySelector('body').classList.add('loaded');
   window.history.pushState({}, null, "/" + "");
}

// Get movies on DOM load
document.addEventListener('DOMContentLoaded', getMovies);
document.querySelector('#movies').addEventListener('click', getMovie);

const http = new EasyHttP;
const ui = new UI;
const api = 'https://backend-ygzsyibiue.now.sh/api/v1/movies/';

function getMovies(){
   http.get(api)
      .then(data => ui.showMovies(data))
      .catch(err => console.log(err))
 }

 function getMovie(e){
   const id = e.target.parentElement.id;
   
   http.get(api+`${id}`)
      .then(data => ui.showMovie(data))
      .catch(err => console.log(err))
 }

